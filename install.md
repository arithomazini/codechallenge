Para configurar e instalar o projeto:

1 - Executar o arquivo configure que está localizado na raiz do projeto:
	sh configure
	Ele irá instalar o gradle e o JDK para compilação do projeto
	
2 - Para iniciar a aplicação:
	Entrar na pasta codeChallenge que foi clonada anteriormente via git
	Executar:
	gradle bootRun (Irá realizar o build e subirá o projeto)
	
3 - Acessar via navegador:
	http://localhost:8080
	
	
	