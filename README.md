# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Projeto desenvolvido como uma desafio de código para a Entelgy

### How do I get set up? ###

Executar arquivo 'configure' na raiz do projeto (sh configure)

* Dependencies
Projeto desenvolvido em Java, utilizando as seguintes tecnologias:

Spring Boot, Spring Security, Spring MVC, Thymeleaf, jQuery, JMS, ActiveMQ, Tomcat, jUnit

Porque escolhi essas tecnologias?

Spring Boot --- Pela facilidade de configuração. Foi utilizado o Spring Initializr para configuração inicial do projeto, visto o tempo de desenvolvimento que teria para o mesmo.
JMS --- Utilizado para realizar a finalização do pedido do usuário, para que se possa aguentar uma quatidade alta de pedidos simultâneos.
Thymeleaf --- Para aprendizado. Como tenho mais vivência com velocity, preferi utilizar o thymeleaf para que pudesse aprender
Spring Security --- Utilizado para a realização do login do usuário, e também para o controle de tipos de usuário que o sistema pode ter.

O que gostaria de implementar se tivesse mais tempo:
- Melhorias no layout, pois com o tempo que pude dispor nessa semana, não consegui fazer um layout a altura do que deveria ter feito e também criar a calculadora na tela final do requisito, como item do mesmo.
- Banco de Dados físico e não embedded. Porém, também devido ao tempo, utilizei ele para ganhar tempo e poder finalizar algumas coisas a mais de regras de negócio.
- Testes automatizados e muito mais JUnits.
- Desenvolvimento baseado em BDD, provavelmente utilizando o Cucumber.
- Internacionalização.

Também fiz a escolha por desenvolver uma parte de redirect por role, para poder ter um acesso controlado dos usuários do sistema.
Com isso, desenvolvi uma parte de um dos requisitos a mais.

### Iniciando o projeto (install) ###

Para configurar e instalar o projeto:

1 - Executar o arquivo configure que está localizado na raiz do projeto:
	sh configure
	Ele irá instalar o maven e o JDK para compilação do projeto

2 - Para iniciar a aplicação:
	Entrar na pasta codeChallenge que foi clonada anteriormente via git
	Executar:
	gradle bootRun (Irá realizar o build e subirá o projeto)

3 - Acessar via navegador:
	http://localhost:8080

4 - EXISTEM DOIS USUÁRIOS JÁ CADASTRADOS, UM DO TIPO 'ADMIN' E OUTRO DO TIPO 'USER'

--ADMIN
Usuário: admin@admin.com
Senha: admin

--USER
Usuário: user@user.com
Senha: user