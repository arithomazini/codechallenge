$(document).ready(function() {
    $("#snack").change(function() {
        var idSnack = $(this).val();
        if(idSnack != 0){
            $.get("/getIngredients/"+idSnack, function(data){
                for (var i = 0; i < data.length; i++){
                    if(data[i].type === "BREAD"){
                        $("#bread").val(data[i].id);
                    }
                    if(data[i].type === "CHEESE"){
                        $("#cheese").val(data[i].id);
                    }
                    if(data[i].type === "FILLING"){
                        $("#filling").val(data[i].id);
                    }
                    if(data[i].type === "SALAD"){
                        $("#salad").val(data[i].id).attr('checked',true);
                    }
                    if(data[i].type === "SAUCE"){
                        $("#sauce").val(data[i].id).attr('checked',true);;
                    }
                    if(data[i].type === "SPICE"){
                        $("#spice").val(data[i].id).attr('checked',true);;
                    }
                }
            })
        }
    })
});