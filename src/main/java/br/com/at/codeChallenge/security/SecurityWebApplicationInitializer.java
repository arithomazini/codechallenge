package br.com.at.codeChallenge.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
public class SecurityWebApplicationInitializer  extends AbstractSecurityWebApplicationInitializer{
}
