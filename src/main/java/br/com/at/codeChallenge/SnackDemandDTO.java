package br.com.at.codeChallenge;

import br.com.at.codeChallenge.entities.Snack;
import br.com.at.codeChallenge.entities.SnackDemand;
import br.com.at.codeChallenge.entities.User;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Ari Thomazini on 08/12/2016.
 */
public class SnackDemandDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<Snack> snacks;

    private User user;

    private String address;

    private BigDecimal totalValue;

    public SnackDemandDTO(User user, List<Snack> snacks, String address, BigDecimal totalValue) {
        this.user = user;
        this.snacks = snacks;
        this.address = address;
        this.totalValue = totalValue;
    }

    public SnackDemand getSnackDemand(){
        SnackDemand snackDemand = new SnackDemand();
        snackDemand.setDateRequest(Calendar.getInstance().getTime());
        snackDemand.setProcessed(false);
        snackDemand.setSnacks(this.snacks);
        snackDemand.setUser(this.user);
        snackDemand.setAddress(this.address);
        snackDemand.setTotalValue(this.totalValue);

        return snackDemand;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Snack> getSnacks() {
        return snacks;
    }

    public void setSnacks(List<Snack> snacks) {
        this.snacks = snacks;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }
}
