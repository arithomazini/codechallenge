package br.com.at.codeChallenge.vo;

/**
 * Created by Ari Thomazini on 10/12/2016.
 */
public class FinishDemandVO {

    private String address;

    public FinishDemandVO() {
    }

    public FinishDemandVO(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
