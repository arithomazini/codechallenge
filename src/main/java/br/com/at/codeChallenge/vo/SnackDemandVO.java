package br.com.at.codeChallenge.vo;

/**
 * Created by Ari Thomazini on 11/12/2016.
 */
public class SnackDemandVO {
    private Integer id;

    public SnackDemandVO() {
    }

    public SnackDemandVO(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
