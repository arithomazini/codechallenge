package br.com.at.codeChallenge.vo;

import br.com.at.codeChallenge.entities.Snack;

import java.math.BigDecimal;

/**
 * Created by Ari Thomazini on 10/12/2016.
 */
public class SnackVO {

    private Snack snack;

    private BigDecimal totalValue;

    public SnackVO() {
    }

    public SnackVO(Snack snack, BigDecimal totalValue) {
        this.snack = snack;
        this.totalValue = totalValue;
    }

    public Snack getSnack() {
        return snack;
    }

    public void setSnack(Snack snack) {
        this.snack = snack;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }
}
