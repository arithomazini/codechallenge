package br.com.at.codeChallenge.vo;

import br.com.at.codeChallenge.entities.Snack;

import java.util.List;

/**
 * Created by Ari Thomazini on 08/12/2016.
 */
public class SnackIngredientsVO {

    private String address;

    private Snack snack;

    private String bread;

    private String cheese;

    private String filling;

    private String salad;

    private String sauce;

    private String spice;

    private boolean doubleCheese;

    private boolean doubleSalad;

    private boolean doubleFilling;

    private List<Snack> snackDemand;

    public SnackIngredientsVO() {
    }

    public SnackIngredientsVO(String address, Snack snack, String bread, String cheese, String filling, String salad, String sauce, String spice, List<Snack> snackDemand, boolean doubleCheese, boolean doubleFilling, boolean doubleSalad) {
        this.address = address;
        this.snack = snack;
        this.bread = bread;
        this.cheese = cheese;
        this.filling = filling;
        this.salad = salad;
        this.sauce = sauce;
        this.spice = spice;
        this.snackDemand = snackDemand;
        this.doubleCheese = doubleCheese;
        this.doubleFilling = doubleFilling;
        this.doubleSalad = doubleSalad;
    }

    public String getBread() {
        return bread;
    }

    public void setBread(String bread) {
        this.bread = bread;
    }

    public String getCheese() {
        return cheese;
    }

    public void setCheese(String cheese) {
        this.cheese = cheese;
    }

    public String getFilling() {
        return filling;
    }

    public void setFilling(String filling) {
        this.filling = filling;
    }

    public String getSalad() {
        return salad;
    }

    public void setSalad(String salad) {
        this.salad = salad;
    }

    public String getSauce() {
        return sauce;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public String getSpice() {
        return spice;
    }

    public void setSpice(String spice) {
        this.spice = spice;
    }

    public List<Snack> getSnackDemand() {
        return snackDemand;
    }

    public void setSnackDemand(List<Snack> snackDemand) {
        this.snackDemand = snackDemand;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Snack getSnack() {
        return snack;
    }

    public void setSnack(Snack snack) {
        this.snack = snack;
    }

    public boolean isDoubleCheese() {
        return doubleCheese;
    }

    public void setDoubleCheese(boolean doubleCheese) {
        this.doubleCheese = doubleCheese;
    }

    public boolean isDoubleSalad() {
        return doubleSalad;
    }

    public void setDoubleSalad(boolean doubleSalad) {
        this.doubleSalad = doubleSalad;
    }

    public boolean isDoubleFilling() {
        return doubleFilling;
    }

    public void setDoubleFilling(boolean doubleFilling) {
        this.doubleFilling = doubleFilling;
    }
}
