package br.com.at.codeChallenge.dao;

import br.com.at.codeChallenge.entities.Ingredient;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
public interface IngredientDAO extends CrudRepository<Ingredient, Integer>{
    Iterable<Ingredient> findAllByType(Ingredient.IngredientType ingredientType);

    Ingredient findByName(String name);
}
