package br.com.at.codeChallenge.dao;

import br.com.at.codeChallenge.entities.SnackDemand;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Ari Thomazini on 08/12/2016.
 */
public interface SnackDemandDAO extends CrudRepository<SnackDemand, Integer> {
    Iterable<SnackDemand> findAllByProcessedFalse();

    @Modifying
    @Query("update SnackDemand sd set sd.processed = true where sd.id = ?1")
    void finishDemand(Integer id);
}
