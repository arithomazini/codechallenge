package br.com.at.codeChallenge.dao;

import br.com.at.codeChallenge.entities.Role;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
public interface RoleDAO extends CrudRepository<Role, String> {
}
