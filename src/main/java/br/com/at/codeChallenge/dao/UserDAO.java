package br.com.at.codeChallenge.dao;

import br.com.at.codeChallenge.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */

@Repository
public interface UserDAO extends CrudRepository<User, Integer>{

    User findByName(String name);

    User findByEmail(String email);
}
