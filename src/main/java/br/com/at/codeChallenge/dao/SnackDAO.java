package br.com.at.codeChallenge.dao;

import br.com.at.codeChallenge.entities.Snack;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
public interface SnackDAO extends CrudRepository<Snack, Integer>{
    Iterable<Snack> findAllByManufacturedTrue();
}
