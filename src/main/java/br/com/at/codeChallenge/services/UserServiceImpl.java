package br.com.at.codeChallenge.services;

import br.com.at.codeChallenge.dao.RoleDAO;
import br.com.at.codeChallenge.dao.UserDAO;
import br.com.at.codeChallenge.entities.User;
import br.com.at.codeChallenge.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
@Service
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private RoleDAO roleDAO;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userDAO.findByEmail(username);

        if(user == null) {
            throw new UsernameNotFoundException("Usuário não encontrado.");
        }

        return user;
    }

    @Override
    public Iterable<User> findAll() {
        return userDAO.findAll();
    }

}
