package br.com.at.codeChallenge.services;

import br.com.at.codeChallenge.dao.*;
import br.com.at.codeChallenge.entities.*;
import br.com.at.codeChallenge.services.interfaces.InicialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
@Service
public class InicialServiceImpl implements InicialService {

    @Autowired
    private SnackDAO snackDAO;

    @Autowired
    private RoleDAO roleDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private IngredientDAO ingredientDAO;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createInitial() {
        userInitialConfig();
        ingredientsInitialConfig();
    }

    @Override
    public void createSnack() {
        //SNACK
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredientDAO.findByName("Pão Francês"));
        ingredients.add(ingredientDAO.findByName("Queijo Minas"));
        ingredients.add(ingredientDAO.findByName("Presunto"));
        ingredients.add(ingredientDAO.findByName("Alface"));
        ingredients.add(ingredientDAO.findByName("Mostarda"));
        ingredients.add(ingredientDAO.findByName("Malagueta"));
        Snack mistoQuente = new Snack("Misto Quente", ingredients, true);

        snackDAO.save(mistoQuente);
    }

    private void ingredientsInitialConfig() {
        //BREAD
        Ingredient paoFrances = new Ingredient("Pão Francês", BigDecimal.ONE, Ingredient.IngredientType.BREAD);
        Ingredient paoDeForma = new Ingredient("Pão de Forma", BigDecimal.TEN, Ingredient.IngredientType.BREAD);

        ingredientDAO.save(paoFrances);
        ingredientDAO.save(paoDeForma);

        //CHEESE
        Ingredient queijoMinas = new Ingredient("Queijo Minas", BigDecimal.ONE, Ingredient.IngredientType.CHEESE);
        Ingredient queijoParmesao = new Ingredient("Queijo Parmesão", BigDecimal.TEN, Ingredient.IngredientType.CHEESE);

        ingredientDAO.save(queijoMinas);
        ingredientDAO.save(queijoParmesao);

        //FILLING
        Ingredient carne = new Ingredient("Carne", BigDecimal.ONE, Ingredient.IngredientType.FILLING);
        Ingredient frango = new Ingredient("Frango", BigDecimal.TEN, Ingredient.IngredientType.FILLING);
        Ingredient presunto = new Ingredient("Presunto", new BigDecimal(5.0), Ingredient.IngredientType.FILLING);

        ingredientDAO.save(carne);
        ingredientDAO.save(frango);
        ingredientDAO.save(presunto);

        //SALAD
        Ingredient alface = new Ingredient("Alface", new BigDecimal(0.1), Ingredient.IngredientType.SALAD);
        Ingredient rucula = new Ingredient("Rúcula", new BigDecimal(0.2), Ingredient.IngredientType.SALAD);

        ingredientDAO.save(alface);
        ingredientDAO.save(rucula);

        //SAUCE
        Ingredient mostarde = new Ingredient("Mostarda", new BigDecimal(0.1), Ingredient.IngredientType.SAUCE);
        Ingredient catchup = new Ingredient("Catchup", new BigDecimal(0.2), Ingredient.IngredientType.SAUCE);

        ingredientDAO.save(mostarde);
        ingredientDAO.save(catchup);

        //SPICE
        Ingredient malagueta = new Ingredient("Malagueta", new BigDecimal(0.1), Ingredient.IngredientType.SPICE);
        Ingredient reino = new Ingredient("Do Reino", new BigDecimal(0.2), Ingredient.IngredientType.SPICE);

        ingredientDAO.save(malagueta);
        ingredientDAO.save(reino);

    }

    private void userInitialConfig() {
        //ROLE
        Role admin = new Role("ROLE_ADMIN");
        Role user = new Role("ROLE_USER");

        roleDAO.save(admin);
        roleDAO.save(user);


        //USER
        List<Role> roles = new ArrayList<Role>();
        roles.add(roleDAO.findOne("ROLE_USER"));
        User usuario = new User("User", "user@user.com", "$2a$08$cW7Ri2yhWV1kSNccEWbXXujjqVsPMmPkfFbIiNetYJQnhdhx6igh.", roles);
        userDAO.save(usuario);


        //ADMIN
        roles = new ArrayList<Role>();
        roles.add(roleDAO.findOne("ROLE_ADMIN"));
        User administrador = new User("Admin", "admin@admin.com", "$2a$08$hGY7KGQf/0VYvhlF3pBa8ONbvGdDWsQqQRJIUmmA4nznd38eOb45S", roles);
        userDAO.save(administrador);
    }
}