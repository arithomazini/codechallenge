package br.com.at.codeChallenge.services.interfaces;

import br.com.at.codeChallenge.entities.SnackDemand;

/**
 * Created by Ari Thomazini on 08/12/2016.
 */
public interface SnackDemandService {
    void saveSnackDemand(SnackDemand snackDemand);

    Iterable<SnackDemand> findAll();

    Iterable<SnackDemand> findAllPending();

    void finishDemand(Integer id);
}
