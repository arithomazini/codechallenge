package br.com.at.codeChallenge.services.interfaces;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
public interface InicialService {
    void createInitial();

    void createSnack();
}
