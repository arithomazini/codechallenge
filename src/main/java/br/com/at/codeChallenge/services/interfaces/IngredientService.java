package br.com.at.codeChallenge.services.interfaces;

import br.com.at.codeChallenge.entities.Ingredient;
import br.com.at.codeChallenge.entities.Snack;
import br.com.at.codeChallenge.entities.User;
import br.com.at.codeChallenge.vo.SnackIngredientsVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
public interface IngredientService {

    Iterable<Ingredient> findAllByType(Ingredient.IngredientType ingredientType);

    Ingredient findByName(String name);

    Ingredient findById(Integer id);

    Iterable<Ingredient> findAll();

    BigDecimal getTotalValueOneSnack(List<Ingredient> ingredients);

    List<Ingredient> getIngredients(SnackIngredientsVO snackVO);
}
