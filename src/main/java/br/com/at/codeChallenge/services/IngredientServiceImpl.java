package br.com.at.codeChallenge.services;

import br.com.at.codeChallenge.dao.IngredientDAO;
import br.com.at.codeChallenge.entities.Ingredient;
import br.com.at.codeChallenge.services.interfaces.IngredientService;
import br.com.at.codeChallenge.vo.SnackIngredientsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
@Service
public class IngredientServiceImpl implements IngredientService {

    @Autowired
    private IngredientDAO ingredientDAO;

    @Override
    public Iterable<Ingredient> findAllByType(Ingredient.IngredientType ingredientType) {
        return ingredientDAO.findAllByType(ingredientType);
    }

    @Override
    public Ingredient findByName(String name) {
        return ingredientDAO.findByName(name);
    }

    @Override
    public Ingredient findById(Integer id) {
        return ingredientDAO.findOne(id);
    }

    @Override
    public Iterable<Ingredient> findAll() {
        return ingredientDAO.findAll();
    }

    @Override
    public BigDecimal getTotalValueOneSnack(List<Ingredient> ingredients) {
        BigDecimal totalValue = BigDecimal.ZERO;
        for (Ingredient ingredient : ingredients) {
            totalValue = totalValue.add(ingredient.getValue());
        }

        return totalValue;
    }

    @Override
    public List<Ingredient> getIngredients(SnackIngredientsVO snackVO) {
        List<Ingredient> ingredients = new ArrayList<>();

        if (snackVO.getBread() != null) {
            ingredients.add(findById(Integer.parseInt(snackVO.getBread())));
        }

        if (snackVO.getCheese() != null) {
            if (snackVO.isDoubleCheese()) {
                ingredients.add(findById(Integer.parseInt(snackVO.getCheese())));
            }
            ingredients.add(findById(Integer.parseInt(snackVO.getCheese())));
        }

        if (snackVO.getFilling() != null) {
            if (snackVO.isDoubleFilling()) {
                ingredients.add(findById(Integer.parseInt(snackVO.getFilling())));
            }
            ingredients.add(findById(Integer.parseInt(snackVO.getFilling())));
        }

        if (snackVO.getSalad() != null) {
            if (snackVO.isDoubleSalad()) {
                ingredients.add(findById(Integer.parseInt(snackVO.getSalad())));
            }
            ingredients.add(findById(Integer.parseInt(snackVO.getSalad())));
        }

        if (snackVO.getSauce() != null) {
            ingredients.add(findById(Integer.parseInt(snackVO.getSauce())));
        }

        if (snackVO.getSpice() != null) {
            ingredients.add(findById(Integer.parseInt(snackVO.getSpice())));
        }

        return ingredients;
    }
}
