package br.com.at.codeChallenge.services;

import br.com.at.codeChallenge.dao.SnackDAO;
import br.com.at.codeChallenge.dao.SnackDemandDAO;
import br.com.at.codeChallenge.entities.Snack;
import br.com.at.codeChallenge.entities.SnackDemand;
import br.com.at.codeChallenge.services.interfaces.SnackDemandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Ari Thomazini on 08/12/2016.
 */
@Service
public class SnackDemandServiceImpl implements SnackDemandService{

    @Autowired
    private SnackDemandDAO snackDemandDAO;

    @Autowired
    private SnackDAO snackDAO;

    @Override
    public void saveSnackDemand(SnackDemand snackDemand) {
        snackDemandDAO.save(snackDemand);
    }

    @Override
    public Iterable<SnackDemand> findAll() {
        return snackDemandDAO.findAll();
    }

    @Override
    public Iterable<SnackDemand> findAllPending() {
        return snackDemandDAO.findAllByProcessedFalse();
    }

    @Override
    @Transactional
    public void finishDemand(Integer id) {
        snackDemandDAO.finishDemand(id);
    }
}
