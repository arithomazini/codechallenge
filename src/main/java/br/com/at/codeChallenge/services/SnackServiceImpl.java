package br.com.at.codeChallenge.services;

import br.com.at.codeChallenge.dao.SnackDAO;
import br.com.at.codeChallenge.entities.Ingredient;
import br.com.at.codeChallenge.entities.Snack;
import br.com.at.codeChallenge.entities.User;
import br.com.at.codeChallenge.jms.SnackDemandProducer;
import br.com.at.codeChallenge.services.interfaces.SnackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
@Service
public class SnackServiceImpl implements SnackService {

    @Autowired
    private SnackDAO snackDAO;

    @Autowired
    private SnackDemandProducer snackDemandProducer;

    @Override
    public Iterable<Snack> findAll() {
        return snackDAO.findAllByManufacturedTrue();
    }

    @Override
    public Snack save(Snack snack) {
        return snackDAO.save(snack);
    }

    @Override
    public void processSnackDemand(List<Snack> snacks, User user, String address, BigDecimal totalValue) {
        snackDemandProducer.sendMessage(snacks, user, address, totalValue);
    }

    @Override
    public Snack findById(Integer id) {
        return snackDAO.findOne(id);
    }

    @Override
    public BigDecimal getTotalValue(List<Snack> snackDemand) {
        BigDecimal totalValue = BigDecimal.ZERO;
        for (Snack snack : snackDemand) {
            for (Ingredient ingredient : snack.getIngredients()) {
                totalValue = totalValue.add(ingredient.getValue());
            }
        }

        return totalValue;
    }

    @Override
    public List<Ingredient> findAllSnackIngredients(Integer id) {
        Snack snack = findById(id);

        return snack.getIngredients();
    }

}