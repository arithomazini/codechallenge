package br.com.at.codeChallenge.services.interfaces;

import br.com.at.codeChallenge.entities.Ingredient;
import br.com.at.codeChallenge.entities.Snack;
import br.com.at.codeChallenge.entities.User;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
public interface SnackService {

    Iterable<Snack> findAll();

    Snack save(Snack snack);

    void processSnackDemand(List<Snack> snackDemand, User user, String address, BigDecimal totalValue);

    Snack findById(Integer id);

    BigDecimal getTotalValue(List<Snack> snackDemand);

    List<Ingredient> findAllSnackIngredients(Integer id);
}
