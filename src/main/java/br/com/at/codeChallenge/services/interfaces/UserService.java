package br.com.at.codeChallenge.services.interfaces;

import br.com.at.codeChallenge.entities.User;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
public interface UserService{

    Iterable<User> findAll();
}
