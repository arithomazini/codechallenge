package br.com.at.codeChallenge.controller;

import br.com.at.codeChallenge.entities.Ingredient;
import br.com.at.codeChallenge.entities.Snack;
import br.com.at.codeChallenge.entities.SnackDemand;
import br.com.at.codeChallenge.entities.User;
import br.com.at.codeChallenge.services.interfaces.IngredientService;
import br.com.at.codeChallenge.services.interfaces.SnackDemandService;
import br.com.at.codeChallenge.services.interfaces.SnackService;
import br.com.at.codeChallenge.vo.FinishDemandVO;
import br.com.at.codeChallenge.vo.SnackIngredientsVO;
import br.com.at.codeChallenge.vo.SnackVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ari Thomazini on 06/12/2016.
 */

@Controller
@RequestMapping(value = "/burguer")
public class BurguerController {
    private static final String MSG02 = "Ao menos um lanche deve ser selecionado para o pedido.";
    private static final String MSG01 = "Existem campos obrigatórios não preenchidos";

    protected Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private SnackService snackService;

    @Autowired
    private IngredientService ingredientService;

    @Autowired
    private SnackDemandService snackDemandService;

    /**
     * Metodo que inicia a tela de requisicao de lanche
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/request", method = RequestMethod.GET)
    public String burguerRequest(Model model, HttpServletRequest request) {
        logger.info("Iniciando a tela de pedido de lanche");
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = user.getName();
        HttpSession session = request.getSession();

        fillModel(model, name, session);
        return "burguer-request";
    }

    /**
     * Metodo que realiza a inclusao de um novo lanche na lista do usuario
     * @param snackVO
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/request", method = RequestMethod.POST)
    public String addSnack(@ModelAttribute SnackIngredientsVO snackVO, Model model, HttpServletRequest request) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = user.getName();
        HttpSession session = request.getSession();
        List<Snack> snackDemand = (List<Snack>) session.getAttribute("snackDemand");

        if(snackVO.getSalad() == null || snackVO.getBread() == null) {
            model.addAttribute("requestError", true);
            model.addAttribute("requestErrorMessage", MSG01);
            fillModelAddSnack(model, name, session, snackDemand);
            return "burguer-request";
        }

        if (snackDemand == null || snackDemand.size() <= 0) {
            snackDemand = new ArrayList<Snack>();
        }

        List<Ingredient> ingredients = ingredientService.getIngredients(snackVO);
        Snack chooseSnack = new Snack(name, ingredients, false);
        snackDemand.add(chooseSnack);

        session.setAttribute("snackDemand", snackDemand);

        model.addAttribute("demandTotalValue", snackService.getTotalValue(snackDemand));
        model.addAttribute("username", name);
        model.addAttribute("breads", ingredientService.findAllByType(Ingredient.IngredientType.BREAD));
        model.addAttribute("cheeses", ingredientService.findAllByType(Ingredient.IngredientType.CHEESE));
        model.addAttribute("fillings", ingredientService.findAllByType(Ingredient.IngredientType.FILLING));
        model.addAttribute("salads", ingredientService.findAllByType(Ingredient.IngredientType.SALAD));
        model.addAttribute("sauces", ingredientService.findAllByType(Ingredient.IngredientType.SAUCE));
        model.addAttribute("spices", ingredientService.findAllByType(Ingredient.IngredientType.SPICE));
        model.addAttribute("snacks", getSnacksAndRespectiveValue());


        model.addAttribute("snackVO", new SnackIngredientsVO());
        model.addAttribute("finishDemandVO", new FinishDemandVO());

        return "burguer-request";
    }

    /**
     * Metodo que finaliza a requisicao da demanda, salvando os snacks para o respectivo usuario e pedido
     * @param vo
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/finishDemand", method = RequestMethod.POST)
    public String finishDemand(@ModelAttribute FinishDemandVO vo, Model model, HttpServletRequest request) {
        logger.info("Iniciando a finalizacao do pedido");
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = user.getName();
        HttpSession session = request.getSession();
        List<Snack> snackDemand = (List<Snack>) session.getAttribute("snackDemand");

        if(snackDemand == null || snackDemand.size() <= 0) {
            model.addAttribute("requestError", true);
            model.addAttribute("requestErrorMessage", MSG02);
            fillModelAddSnack(model, name, session, snackDemand);
            return "burguer-request";
        }
        if(vo.getAddress() == null || vo.getAddress().isEmpty()) {
            model.addAttribute("requestError", true);
            model.addAttribute("requestErrorMessage", MSG01);
            fillModelAddSnack(model, name, session, snackDemand);
            return "burguer-request";
        }

        logger.info("Enfileirando mensagem para ser processada por uma fila");
        BigDecimal totalValue = snackService.getTotalValue(snackDemand);
        snackService.processSnackDemand(snackDemand, user, vo.getAddress(), totalValue);

        model.addAttribute("name", name);
        model.addAttribute("totalValue", totalValue.toEngineeringString());

        return "burguer-finish";
    }

    private void fillModelAddSnack(Model model, String name, HttpSession session, List<Snack> snackDemand) {
        session.setAttribute("snackDemand", snackDemand);
        model.addAttribute("demandTotalValue", snackService.getTotalValue(snackDemand));
        model.addAttribute("username", name);
        model.addAttribute("breads", ingredientService.findAllByType(Ingredient.IngredientType.BREAD));
        model.addAttribute("cheeses", ingredientService.findAllByType(Ingredient.IngredientType.CHEESE));
        model.addAttribute("fillings", ingredientService.findAllByType(Ingredient.IngredientType.FILLING));
        model.addAttribute("salads", ingredientService.findAllByType(Ingredient.IngredientType.SALAD));
        model.addAttribute("sauces", ingredientService.findAllByType(Ingredient.IngredientType.SAUCE));
        model.addAttribute("spices", ingredientService.findAllByType(Ingredient.IngredientType.SPICE));
        model.addAttribute("snacks", getSnacksAndRespectiveValue());
        model.addAttribute("snackVO", new SnackIngredientsVO());
        model.addAttribute("finishDemandVO", new FinishDemandVO());
        model.addAttribute("requestError", false);
        model.addAttribute("requestErrorMessafe", "");

    }

    private void fillModel(Model model, String name, HttpSession session) {
        model.addAttribute("username", name);
        model.addAttribute("breads", ingredientService.findAllByType(Ingredient.IngredientType.BREAD));
        model.addAttribute("cheeses", ingredientService.findAllByType(Ingredient.IngredientType.CHEESE));
        model.addAttribute("fillings", ingredientService.findAllByType(Ingredient.IngredientType.FILLING));
        model.addAttribute("salads", ingredientService.findAllByType(Ingredient.IngredientType.SALAD));
        model.addAttribute("sauces", ingredientService.findAllByType(Ingredient.IngredientType.SAUCE));
        model.addAttribute("spices", ingredientService.findAllByType(Ingredient.IngredientType.SPICE));
        model.addAttribute("snacks", getSnacksAndRespectiveValue());
        model.addAttribute("snackVO", new SnackIngredientsVO());
        model.addAttribute("finishDemandVO", new FinishDemandVO());
        model.addAttribute("demandTotalValue", BigDecimal.ZERO.toEngineeringString());
        session.setAttribute("snackDemand", new ArrayList<>());
        model.addAttribute("requestError", false);
        model.addAttribute("requestErrorMessafe", "");
    }


    /**
     * Metodo usado para simples conferencia
     * @return
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.GET)
    @ResponseBody
    public String listAll() {

        Iterable<Ingredient> ingredients = ingredientService.findAll();

        Iterable<SnackDemand> snackDemands = snackDemandService.findAll();

        return "Olá";

    }

    private List<SnackVO> getSnacksAndRespectiveValue() {

        List<SnackVO> snackVOs = new ArrayList<>();

        Iterable<Snack> snacks = snackService.findAll();

        for (Snack snack : snacks) {
            SnackVO snackVO = new SnackVO();
            snackVO.setSnack(snack);
            snackVO.setTotalValue(ingredientService.getTotalValueOneSnack(snack.getIngredients()));

            snackVOs.add(snackVO);
        }
        return snackVOs;
    }


}
