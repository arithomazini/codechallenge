package br.com.at.codeChallenge.controller;

import br.com.at.codeChallenge.entities.Ingredient;
import br.com.at.codeChallenge.services.interfaces.SnackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Ari Thomazini on 12/12/2016.
 */
@RestController
public class AjaxController {
    @Autowired
    private SnackService snackService;

    @RequestMapping("/getIngredients/{id}")
    public List<Ingredient> getIngredients(@PathVariable Integer id){
        return snackService.findAllSnackIngredients(id);
    }
}
