package br.com.at.codeChallenge.controller;

import br.com.at.codeChallenge.entities.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
@Controller
@RequestMapping(value = "/signup")
public class UserController {

    @RequestMapping(method = RequestMethod.GET)
    public String signup(Model model){

        model.addAttribute("user", new User());

        return "signup";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String signupPost(@ModelAttribute User user, @RequestParam String passwordRetype){


        return "signup";
    }
}
