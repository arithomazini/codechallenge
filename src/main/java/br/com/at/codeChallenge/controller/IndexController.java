package br.com.at.codeChallenge.controller;

import br.com.at.codeChallenge.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Ari Thomazini on 06/12/2016.
 */

@Controller
public class IndexController {

    @Autowired
    private UserService userService;

    @RequestMapping(value= {"/","/home"}, method= RequestMethod.GET)
    public String index(Model model){
        return "index";
    }

    @RequestMapping(value= {"/fail"}, method= RequestMethod.GET)
    public String failLogin(Model model, HttpServletResponse resp) throws IOException {
        model.addAttribute("loginError", true);
        return "/index";
    }


}
