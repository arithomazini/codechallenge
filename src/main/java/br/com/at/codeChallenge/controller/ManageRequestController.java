package br.com.at.codeChallenge.controller;

import br.com.at.codeChallenge.services.interfaces.SnackDemandService;
import br.com.at.codeChallenge.vo.SnackDemandVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Ari Thomazini on 11/12/2016.
 */

@Controller
@RequestMapping(value = "/manage")
public class ManageRequestController {
    protected Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private SnackDemandService snackDemandService;

    @RequestMapping(value = "/request", method = RequestMethod.GET)
    public String burguerRequest(Model model, HttpServletRequest request) {

        model.addAttribute("pendingDemands", snackDemandService.findAllPending());
        model.addAttribute("snackDemandVO", new SnackDemandVO());

        return "manageRequest";
    }

    @RequestMapping(value = "/closeDemand", method = RequestMethod.POST)
    public String closeDemand(@ModelAttribute SnackDemandVO demand, Model model, HttpServletRequest request) {
        logger.info("Finalizando o pedido: "+ demand.getId());

        snackDemandService.finishDemand(demand.getId());

        model.addAttribute("pendingDemands", snackDemandService.findAllPending());
        model.addAttribute("snackDemandVO", new SnackDemandVO());

        return "manageRequest";
    }

}
