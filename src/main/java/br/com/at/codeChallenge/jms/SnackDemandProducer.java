package br.com.at.codeChallenge.jms;

import br.com.at.codeChallenge.SnackDemandDTO;
import br.com.at.codeChallenge.entities.Snack;
import br.com.at.codeChallenge.entities.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Ari Thomazini on 08/12/2016.
 */
@Component
public class SnackDemandProducer {
    protected Log logger = LogFactory.getLog(this.getClass());
    public static final String SNACK_DEMAND_QUEUE = "queue.snackDemand";

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendMessage(List<Snack> snacks, User user, String address, BigDecimal totalValue) {
        logger.info("Enviando mensagem para a fila");
        SnackDemandDTO snackDemandDTO = new SnackDemandDTO(user, snacks, address, totalValue);
        jmsTemplate.send(SNACK_DEMAND_QUEUE, createMessage(snackDemandDTO));
    }

    private MessageCreator createMessage(final SnackDemandDTO snackDemandDTO) {
        return new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createObjectMessage(snackDemandDTO);
            }
        };
    }
}
