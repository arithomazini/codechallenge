package br.com.at.codeChallenge.jms;

import br.com.at.codeChallenge.SnackDemandDTO;
import br.com.at.codeChallenge.entities.Snack;
import br.com.at.codeChallenge.entities.SnackDemand;
import br.com.at.codeChallenge.services.interfaces.SnackDemandService;
import br.com.at.codeChallenge.services.interfaces.SnackService;
import org.apache.activemq.command.ActiveMQObjectMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.List;

/**
 * Created by Ari Thomazini on 08/12/2016.
 */
@Component
public class SnackDemandConsumer {
    protected Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private SnackDemandService snackDemandService;

    @Autowired
    private SnackService snackService;

    @JmsListener(destination = SnackDemandProducer.SNACK_DEMAND_QUEUE, containerFactory = "jmsContainerFactory")
    public void onMessage(Message o) throws JMSException {
        logger.info("Consumindo mensagem");

        ActiveMQObjectMessage message = (ActiveMQObjectMessage) o;
        SnackDemand snackDemand = ((SnackDemandDTO) message.getObject()).getSnackDemand();
        logger.info("Verificando os lanches que não existem, e salvando os mesmos.");
        verfiyAndSaveSnacks(snackDemand.getSnacks());
        snackDemandService.saveSnackDemand(snackDemand);
    }

    private void verfiyAndSaveSnacks(List<Snack> snacks) {
        for (Snack snack :
                snacks) {
            if (snack.getId() == null) {
                snackService.save(snack);
            }
        }
    }
}
