package br.com.at.codeChallenge.entities;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
@Entity
public class Role implements GrantedAuthority, Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    private String role;

    public Role(String role) {
        this.role = role;
    }

    public Role() {
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return this.role;
    }
}
