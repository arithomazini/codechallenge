package br.com.at.codeChallenge.entities;

import org.hibernate.annotations.IndexColumn;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
@Entity
public class Snack implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String name;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.DETACH})
    private List<Ingredient> ingredients;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private SnackDemand snackDemands;

    @Column
    private boolean manufactured;

    public Snack(String name, List<Ingredient> ingredients, boolean manufactured) {
        this.name = name;
        this.ingredients = ingredients;
        this.manufactured = manufactured;
    }

    public Snack() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public boolean isManufactured() {
        return manufactured;
    }

    public void setManufactured(boolean manufactured) {
        this.manufactured = manufactured;
    }
}
