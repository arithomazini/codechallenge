package br.com.at.codeChallenge.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Ari Thomazini on 08/12/2016.
 */
@Entity
public class SnackDemand {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    private List<Snack> snacks;

    @Column
    private String address;

    @Column
    private Date dateRequest;

    @Column
    private boolean processed;

    @Column
    private BigDecimal totalValue;

    public SnackDemand() {
    }

    public SnackDemand(User user, List<Snack> snacks, String address, Date dateRequest, boolean processed, BigDecimal totalValue) {
        this.user = user;
        this.snacks = snacks;
        this.address = address;
        this.dateRequest = dateRequest;
        this.processed = processed;
        this.totalValue = totalValue;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Snack> getSnacks() {
        return snacks;
    }

    public void setSnacks(List<Snack> snacks) {
        this.snacks = snacks;
    }

    public Date getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(Date dateRequest) {
        this.dateRequest = dateRequest;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }
}