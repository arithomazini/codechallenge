package br.com.at.codeChallenge.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
@Entity
public class Ingredient implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String name;

    @Enumerated(EnumType.STRING)
    @Column
    private IngredientType type;

    @Column
    private BigDecimal value;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "ingredients")
    private List<Snack> snacks;

    public Ingredient() {
    }

    public Ingredient(String name, BigDecimal value, IngredientType type) {
        this.value = value;
        this.name = name;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public IngredientType getType() {
        return type;
    }

    public void setType(IngredientType type) {
        this.type = type;
    }

    public List<Snack> getSnacks() {
        return snacks;
    }

    public void setSnacks(List<Snack> snacks) {
        this.snacks = snacks;
    }

    public enum IngredientType {
        BREAD, CHEESE, FILLING, SALAD, SAUCE, SPICE
    }
}
