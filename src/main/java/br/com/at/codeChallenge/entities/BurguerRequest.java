package br.com.at.codeChallenge.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Ari Thomazini on 07/12/2016.
 */
@Entity
public class BurguerRequest implements Serializable{

    private static final long serialVersionUID = 1L;

    private static final int MAX_VALUE = 100;

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    @Size(max = MAX_VALUE)
    private String deliveryAddress;

    public BurguerRequest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
