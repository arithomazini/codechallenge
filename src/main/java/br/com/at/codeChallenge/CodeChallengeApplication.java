package br.com.at.codeChallenge;

import br.com.at.codeChallenge.services.interfaces.InicialService;
import br.com.at.codeChallenge.services.interfaces.RoleService;
import br.com.at.codeChallenge.services.interfaces.UserService;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.SimpleJmsListenerContainerFactory;

import javax.jms.ConnectionFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class CodeChallengeApplication implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	private RoleService roleService;

	@Autowired
	private UserService userService;

	@Autowired
	private InicialService inicialService;

	public static void main(String[] args) {
		SpringApplication.run(CodeChallengeApplication.class, args);
	}

	@Bean
	JmsListenerContainerFactory<?> jmsContainerFactory(ConnectionFactory connectionFactory) {
		SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory);
		return factory;
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		inicialService.createInitial();
		inicialService.createSnack();
	}
}
