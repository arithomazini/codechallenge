package br.com.at.codeChallenge;

import br.com.at.codeChallenge.controller.BurguerController;
import br.com.at.codeChallenge.dao.IngredientDAO;
import br.com.at.codeChallenge.entities.Ingredient;
import br.com.at.codeChallenge.entities.Snack;
import br.com.at.codeChallenge.services.IngredientServiceImpl;
import br.com.at.codeChallenge.services.interfaces.IngredientService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ari Thomazini on 11/12/2016.
 */

public class IngredientServiceTest {
    @InjectMocks
    IngredientService ingredientService = new IngredientServiceImpl();

    @Mock
    IngredientDAO ingredientDAO;

    List<Ingredient> ingredients = new ArrayList<>();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        Ingredient paoFrances = new Ingredient("Pão Francês", BigDecimal.ONE, Ingredient.IngredientType.BREAD);
        Ingredient paoDeForma = new Ingredient("Pão de Forma", BigDecimal.TEN, Ingredient.IngredientType.BREAD);
        Ingredient carne = new Ingredient("Carne", BigDecimal.ONE, Ingredient.IngredientType.FILLING);
        Ingredient frango = new Ingredient("Frango", BigDecimal.TEN, Ingredient.IngredientType.FILLING);
        Ingredient presunto = new Ingredient("Presunto", new BigDecimal(5.0), Ingredient.IngredientType.FILLING);

        ingredients.add(paoFrances);
        ingredients.add(paoDeForma);
        ingredients.add(carne);
        ingredients.add(frango);
        ingredients.add(presunto);
    }

    @Test
    public void testGetTotalValueOneSnack() {
        Assert.assertEquals(ingredientService.getTotalValueOneSnack(ingredients), new BigDecimal(27.0));
    }
}
