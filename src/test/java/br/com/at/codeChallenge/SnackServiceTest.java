package br.com.at.codeChallenge;

import br.com.at.codeChallenge.controller.BurguerController;
import br.com.at.codeChallenge.dao.IngredientDAO;
import br.com.at.codeChallenge.entities.Ingredient;
import br.com.at.codeChallenge.entities.Snack;
import br.com.at.codeChallenge.services.SnackServiceImpl;
import br.com.at.codeChallenge.services.interfaces.SnackService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ari Thomazini on 11/12/2016.
 */

public class SnackServiceTest {
    @InjectMocks
    SnackService snackService = new SnackServiceImpl();

    @Mock
    IngredientDAO ingredientDAO;

    List<Ingredient> ingredients = new ArrayList<>();

    Snack snack1, snack2 = null;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        Ingredient paoFrances = new Ingredient("Pão Francês", BigDecimal.ONE, Ingredient.IngredientType.BREAD);
        Ingredient paoDeForma = new Ingredient("Pão de Forma", BigDecimal.TEN, Ingredient.IngredientType.BREAD);
        Ingredient carne = new Ingredient("Carne", BigDecimal.ONE, Ingredient.IngredientType.FILLING);
        Ingredient frango = new Ingredient("Frango", BigDecimal.TEN, Ingredient.IngredientType.FILLING);
        Ingredient presunto = new Ingredient("Presunto", new BigDecimal(5.0), Ingredient.IngredientType.FILLING);

        ingredients.add(paoFrances);
        ingredients.add(paoDeForma);
        ingredients.add(carne);
        ingredients.add(frango);
        ingredients.add(presunto);
        snack1 = new Snack("Lanche Teste", ingredients, false);
        snack2 = new Snack("Lanche Teste 2", ingredients, false);
    }

    @Test
    public void testGetTotalValue() {
        List<Snack> snacks = new ArrayList<>();
        snacks.add(snack1);
        snacks.add(snack2);

        Assert.assertEquals(snackService.getTotalValue(snacks), new BigDecimal(54.0));
    }
}
